package com.vazhasapp.assignment16.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.vazhasapp.assignment16.adapter.PersonsRecyclerAdapter
import com.vazhasapp.assignment16.databinding.FragmentPersonsBinding
import com.vazhasapp.assignment16.viewModel.PersonViewModel

class PersonsFragment : Fragment() {

    private var _binding: FragmentPersonsBinding? = null
    private val binding get() = _binding!!

    private val personViewModel: PersonViewModel by viewModels()

    private val myAdapter = PersonsRecyclerAdapter()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPersonsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        setupRecyclerView()
        listeners()
        personViewModel.read()
    }

    private fun listeners() {
        personViewModel.person.observe(viewLifecycleOwner, {
            myAdapter.setData(it)
        })
    }

    private fun setupRecyclerView() {
        binding.myRecyclerView.apply {
            adapter = myAdapter
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}