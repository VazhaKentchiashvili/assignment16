package com.vazhasapp.assignment16.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vazhasapp.assignment16.R
import com.vazhasapp.assignment16.databinding.FragmentMainBinding
import com.vazhasapp.assignment16.db.Person
import com.vazhasapp.assignment16.viewModel.PersonViewModel

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private val viewModel: PersonViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {

        binding.btnWrite.setOnClickListener {
            viewModel.write(getEnteredPerson())
        }

        binding.btnRead.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_personsFragment)
        }
    }


    private fun getEnteredPerson(): Person {
        val firstName = binding.etFirstName.text.toString()
        val lastName = binding.etLastName.text.toString()
        val age = binding.etAge.text.toString().toInt()
        val height = binding.etHeight.text.toString().toFloat()
        val address = binding.etAddress.text.toString()
        val profileImage = binding.etProfile.text.toString()

       return Person(0, firstName, lastName, age, address, height, profileImage)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}