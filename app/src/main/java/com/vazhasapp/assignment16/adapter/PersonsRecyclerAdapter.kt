package com.vazhasapp.assignment16.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.vazhasapp.assignment16.R
import com.vazhasapp.assignment16.databinding.CustomPersonCardViewBinding
import com.vazhasapp.assignment16.db.Person

class PersonsRecyclerAdapter :
    RecyclerView.Adapter<PersonsRecyclerAdapter.PersonRecyclerViewHolder>() {

    private val personList = mutableListOf<Person>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PersonRecyclerViewHolder(
            CustomPersonCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: PersonRecyclerViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = personList.size

    inner class PersonRecyclerViewHolder(private val binding: CustomPersonCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var currentPerson: Person

        fun bind() {
            currentPerson = personList[adapterPosition]
            binding.tvFirstName.text = currentPerson.firstName
            binding.tvLastName.text = currentPerson.lastName
            binding.tvAge.text = currentPerson.age.toString()
            binding.tvHeight.text = currentPerson.height.toString()
            binding.tvAddress.text = currentPerson.address

            Glide.with(binding.imProfile.context)
                .load(currentPerson.profile.toString())
                .placeholder(R.drawable.ic_launcher_background)
                .into(binding.imProfile)

        }
    }

    fun setData(person: List<Person>) {
        personList.clear()
        personList.addAll(person)
        notifyDataSetChanged()
    }
}