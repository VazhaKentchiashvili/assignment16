package com.vazhasapp.assignment16.db

import androidx.room.Room
import com.vazhasapp.assignment16.App

object DatabaseBuild {

    val db = Room.databaseBuilder(
        App.context!!,
        PersonDatabase::class.java,
        "person_db"
    ).build()
}