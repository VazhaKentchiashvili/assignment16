package com.vazhasapp.assignment16.db

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "person_table")
data class Person(

    @PrimaryKey(autoGenerate = true)
    val id: Int,

    @ColumnInfo(name = "first_name") val firstName: String?,
    @ColumnInfo(name = "last_name") val lastName: String?,
    val age: Int?,
    val address: String?,
    val height: Float?,
    val profile: String?,
) {
    constructor(
        firstName: String?,
        lastName: String?,
        age: Int?,
        address: String?,
        height: Float?,
        profile: String?
    ) : this(0, firstName, lastName, age, address, height, profile)
}
