package com.vazhasapp.assignment16.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vazhasapp.assignment16.db.DatabaseBuild
import com.vazhasapp.assignment16.db.Person
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PersonViewModel : ViewModel() {

    private val _person = MutableLiveData<List<Person>>()
    val person: LiveData<List<Person>> = _person

    fun write(
        person: Person
    ) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                DatabaseBuild.db.personDao()
                    .insertPerson(person)
            }
        }
    }

    fun read() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _person.postValue(DatabaseBuild.db.personDao().getAllPerson())
            }
        }
    }
}